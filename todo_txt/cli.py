import click
from .todo import TODO

# import version info
try:
    from ._version import version as __version__ # type: ignore
except ImportError or ModuleNotFoundError:
    __version__ = "dev"

@click.group()
@click.option('--verbose/--no-verbose', '-v', default=False, help='output debug information')
@click.option('--host', '-h', envvar='OPENPROJECT_HOST', required=True, help='OpenProject host')
@click.option('--api-key', '-k', envvar='OPENPROJECT_API_KEY', required=True, help='OpenProject api-key')
@click.option('--todotxt-file', '-f', envvar='TODO_TXT_FILE', required=True, help='todo txt file')
@click.option('--user', '-u', envvar='TODO_USER', required=True, help='OpenProject user')
def sync_tasks(verbose, host, api_key, todotxt_file, user):
    global todo

    todo = TODO(
        verbose=verbose,
        api_key=api_key,
        host=host,
        user=user,
        todotxt_file=todotxt_file
    )


@sync_tasks.command()
def load_todos():
    print("Loading tasks from OpenProject...")
    with open('todo.txt', 'w', encoding="utf-8") as reader:
        reader.write(todo.get_todos_from_api_key())


@sync_tasks.command()
@click.option('--query', '-q', envvar='TODO_QUERY', required=True, help='OpenProject query to load tasks from OpenProject')
def load_query(query):
    print(f"Loading tasks from OpenProject with query: {query}")
    with open('todo.txt', 'w', encoding="utf-8") as reader:
        reader.write(todo.get_todos_from_query(query))


@sync_tasks.command()
def sync_todos():
    print("Syncing todos to OpenProject...")
    todo.sync_todos_to_openproject()


@sync_tasks.command()
def full_sync():
    print("Sync to OpenProject and load tasks including new ones from OpenProject...")
    todo.sync_todos_to_openproject()
    with open('todo.txt', 'w', encoding="utf-8") as reader:
        reader.write(todo.get_todos_from_api_key())
