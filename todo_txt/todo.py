import datetime
import json
import requests


class TODO:

    def __init__(self, verbose, host, user, api_key, todotxt_file):
        self.verbose = verbose
        self.api_key = api_key
        self.todotxt_file = todotxt_file
        self.host = host
        self.user = user

    def create_request(self, query):
        session = requests.Session()
        session.auth = (self.user, self.api_key)
        response = session.get("https://" + self.host + query, auth=(self.user, self.api_key))
        if self.verbose:
            print(f"[DEBUG] Request: {query}, Response-status_code: {response.status_code}")
        session.close()
        content = json.loads(response.content.decode('utf8'))

        return content

    def check_for_match(self, search, content):
        if self.verbose:
            print(f"[DEBUG] Searching for {search}...")
        for user in content["_embedded"]["elements"]:
            if search == user["name"] or user["name"].lower().__contains__(search.lower()):
                search = (user["id"], user["name"], user["_type"])
                if self.verbose:
                    print(f"[DEBUG] Found {search} => {user}")
                break

        if self.verbose and not isinstance(search, tuple):
            print(f"[DEBUG] {search} not found")
        return search

    def sync_todos_to_openproject(self):
        # load from todo.txt and store each line in list
        with open('todo.txt', 'r', encoding="utf-8") as reader:
            lines = reader.readlines()

        for line in lines:
            skip = False
            if line.__contains__("wp:"):
                skip = True

            # if re.match(r"^\d{4}-\d{2}-\d{2}$", line_list[0]):
            #    skip = True

            if skip:
                if self.verbose:
                    print(f"[DEBUG] Skipping {line} - already synced")
                continue
            else:
                if self.verbose:
                    print(f"[DEBUG] Processing {line}")

                if self.verbose:
                    print(f"[DEBUG] Splitting line... Syntax should be: \n"
                          f"*title.replace.space.with.dots* +project @assignee va:responsible or \n"
                          f"*title.replace.space.with.dots* +project @assignee due:YYYY-MM-DD va:responsible \n")
                line_list = line.split(" ")
                # get subject
                wp_subject = line_list[0].replace(".", " ").strip()
                # get project
                wp_project = line_list[1].replace("+", "").strip()
                # get assignee
                wp_assignee = line_list[2].replace("@", "").replace(".", " ").strip()
                wp_due_date = None

                if line.__contains__("due:"):
                    wp_due_date = line_list[3].replace("due:", "").strip()
                    wp_responsible = line_list[4].replace("va:", "").strip()
                else:
                    # get responsible
                    wp_responsible = line_list[3].replace("va:", "").strip()

                if self.verbose:
                    print(f"[DEBUG] Subject: {wp_subject}, Project: {wp_project}, Assignee: {wp_assignee}, "
                          f"Responsible: {wp_responsible}, Due Date: {wp_due_date}")

                # check if project exists
                query = f"/api/v3/projects/{wp_project}"
                if self.verbose:
                    print(f"[DEBUG] Request to check if project exists: {query}")
                content = self.create_request(query)

                if content["_type"] == "Error":
                    print(f"[ERROR] Project {wp_project} not found")
                    return

                # check if available_assignee contains assignee
                if self.verbose:
                    print(f"[DEBUG] Checking if {wp_assignee} is available...")
                content = self.create_request(f"/api/v3/projects/{wp_project}/available_assignees")
                # check if wp_assignee is in content
                wp_assignee = self.check_for_match(wp_assignee, content)

                if self.verbose:
                    print(f"[DEBUG] Checking if {wp_responsible} is available...")
                # check if available_responsibles contains responsible
                content = self.create_request(f"/api/v3/projects/{wp_project}/available_responsibles")
                # check if wp_responsible is in content
                wp_responsible = self.check_for_match(wp_responsible, content)

                # check if wp_responsible is not a tuple => no match found
                if not isinstance(wp_responsible, tuple):
                    if self.verbose:
                        print(f"[DEBUG] {wp_responsible} not found. Probably not available for this project.")
                    print("Error: va invalid")
                    return
                # check if wp_assignee is not a tuple => no match found
                if not isinstance(wp_assignee, tuple):
                    if self.verbose:
                        print(f"[DEBUG] {wp_assignee} not found. Probably not available for this project.")
                    print("Error: assignee invalid")
                    return

                if self.verbose:
                    print(f"[DEBUG] Checking if wp_assignee is user...")
                if wp_assignee[2] == "User":
                    if self.verbose:
                        print(f"[DEBUG] wp_assignee is user")
                    wp_assignee_type = "users"
                else:
                    if self.verbose:
                        print(f"[DEBUG] wp_assignee is group")
                    wp_assignee_type = "groups"

                if self.verbose:
                    print(f"[DEBUG] Checking if wp_responsible is user...")
                if wp_responsible[2] == "User":
                    if self.verbose:
                        print(f"[DEBUG] wp_responsible is user")
                    wp_responsible_type = "users"
                else:
                    if self.verbose:
                        print(f"[DEBUG] wp_responsible is group")
                    wp_responsible_type = "groups"

                if self.verbose:
                    print(f"[DEBUG] Prepare payload...")
                current_date = datetime.datetime.now().strftime("%Y-%m-%d")
                payload = {
                    "_links": {
                        "assignee": {
                            "href": f"/api/v3/{wp_assignee_type}/{wp_assignee[0]}",
                            "id": wp_assignee[0],
                            "name": wp_assignee[1],
                            "title": wp_assignee[1]
                        },
                        "priority": {
                            "href": "/api/v3/priorities/8"
                        },
                        "responsible": {
                            "href": f"/api/v3/{wp_responsible_type}/{wp_responsible[0]}",
                            "id": wp_responsible[0],
                            "name": wp_responsible[1],
                            "title": wp_responsible[1]
                        },
                        "status": {
                            "href": "/api/v3/statuses/1"
                        },
                        "type": {
                            "href": "/api/v3/types/1"
                        },
                        "version": {
                            "href": None
                        }
                    },
                    "description": {
                        "format": "textile",
                        "html": "",
                        "raw": "synced by todo-txt.py"
                    },
                    "dueDate": wp_due_date,
                    "estimatedTime": None,
                    "lockVersion": 0,
                    "parentId": None,
                    "percentageDone": 0,
                    "remainingTime": None,
                    "startDate": current_date,
                    "subject": wp_subject
                }

                if self.verbose:
                    print(f"[DEBUG] Create request with payload: {payload}")

                response = requests.post(
                    f'https://{self.host}/api/v3/projects/{wp_project}/work_packages',
                    headers={
                        'Content-Type': 'application/json'
                    },
                    auth=(self.user, self.api_key),
                    data=json.dumps(payload)
                )

                if self.verbose:
                    print(f"[DEBUG] Request to create work_package: /api/v3/projects/{wp_project}/work_packages, "
                          f"Response: {response.status_code}")

    def get_todos_from_query(self, query):
        """Get todos from query"""
        if self.verbose:
            print(f"[DEBUG] Get todos from query: {query}")
        content = self.create_request(query)
        todo_string = ""

        if self.verbose:
            print(f"[DEBUG] Parse content...")
        for todo in content["_embedded"]["elements"]:
            start_date = todo["startDate"]
            title = todo["subject"]
            project_link = (todo["_links"]["project"]["href"]).split("/")
            project_id = project_link[len(project_link) - 1]
            assignee = todo["_links"]["assignee"]["title"]
            work_package_id = todo["id"]
            due_date = todo["dueDate"]
            if due_date:
                due_date = f" due:{due_date}"
            else:
                due_date = ""

            # check if dict has key title
            if "title" in todo["_links"]["responsible"]:
                responsible = f" va:{todo['_links']['responsible']['title']}"
            else:
                responsible = ""

            if self.verbose:
                print(f"[DEBUG] Add todo to todo_string: \n {start_date} {title} +{project_id} @{assignee} "
                      f"due:{due_date}{responsible}")

            todo_string = todo_string + f"{start_date} {title} +{project_id} @{assignee} " \
                                        f"wp:{work_package_id}{due_date}" \
                                        f"{responsible} \n"

        if self.verbose:
            print(f"[DEBUG] Return todo_string: \n{todo_string}")
        return todo_string

    def get_todos_from_api_key(self):
        query = "/api/v3/queries/138"
        if self.verbose:
            print(f"[DEBUG] Request to load todos: {query}")
        content = self.create_request(query)

        todo_string = ""

        if self.verbose:
            print(f"[DEBUG] Parse content...")
        for todo in content["_embedded"]["results"]["_embedded"]["elements"]:
            start_date = todo["startDate"]
            title = todo["subject"]
            project_link = (todo["_links"]["project"]["href"]).split("/")
            project_id = project_link[len(project_link) - 1]
            assignee = todo["_links"]["assignee"]["title"]
            work_package_id = todo["id"]
            due_date = todo["dueDate"]
            if due_date:
                due_date = f" due:{due_date}"
            else:
                due_date = ""

            # check if dict has key title
            if "title" in todo["_links"]["responsible"]:
                responsible = f" va:{todo['_links']['responsible']['title']}"
            else:
                responsible = ""

            if self.verbose:
                print(f"[DEBUG] Add todo to todo_string: \n {start_date} {title} +{project_id} @{assignee} "
                      f"due:{due_date}{responsible}")

            todo_string = todo_string + f"{start_date} {title} +{project_id} @{assignee} " \
                                        f"wp:{work_package_id}{due_date}" \
                                        f"{responsible} \n"

        if self.verbose:
            print(f"[DEBUG] Return todo_string: \n{todo_string}")
        return todo_string
